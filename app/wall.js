$('.grid').masonry({
  itemSelector: '.grid-item',
  columnWidth: 400
});

function imgError(image) {
  image.onerror = "";
  image.src = "/images/placeholder-img.png";
  return true;
}

angular.module('pinterestly')
  .controller('WallController', function ($scope, $http, $routeParams, Session) {
    $scope.uid = $routeParams.uid;
    $scope.isAdmin = function () {
      return Session.getCurrentUser() && Session.getCurrentUser().uid === $scope.uid;
    };
    var params = $scope.uid ? {"createdUser.uid": $scope.uid} : {};
    var load = function () {
      $http.get('/api/pin', {params: params}).then(function (res) {
        console.log(res);
        $scope.pins = res.data;
      });
    };
    load();

    $scope.destroy = function (pin) {
      $http.delete('/api/pin/' + pin.id).then(function success(res) {
        $scope.message = {
          type: 'success',
          text: 'Deleted successfully'
        };
        load()
      }, function error(res) {
        $scope.message = {
          type: 'danger',
          text: 'Failed'
        };
        load()
      })
    }
  })
  .directive('pin', function () {
    return {
      restrict: 'E',
      templateUrl: 'pin.html',
      transclude: true
    };
  });
