angular.module('pinterestly').factory('Auth', function ($http, $window, Session) {
  return {
    login: function() {
      $window.location.href = '/api/auth/twitter';
    },
    getCurrentUser: function () {
      return Session.getCurrentUser();
    },
    logout: function () {
      $http.get('/api/auth/logout').then(function (res) {
        console.log(res);
        Session.clear();
      }, function (res) {
        console.log(res)
      })
    }
  }
}).factory('Session', function($cookies) {
  var currentUser = null;
  return {
    getCurrentUser: function() {
      return currentUser;
    },
    load: function() {
      var user = $cookies.get('user');
      currentUser = user ? JSON.parse(user) : null;
    },
    clear: function() {
      $cookies.remove('user');
      currentUser = null;
    }
  }
})
;


