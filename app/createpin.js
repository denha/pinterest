angular.module('pinterestly')
  .controller('PinController', function ($scope, $http) {
  $scope.message = null;
  $scope.create = function() {
    console.log($scope.title, $scope.url)
    $http.post('/api/pin', {title: $scope.title, url: $scope.url}).then(function success(res) {
      console.log(res);
      $scope.message = {
        type: 'success',
        text: 'Added successfully'
      };
    }, function error(res) {
      console.log(res);
      $scope.message = {
        type: 'danger',
        text: 'Failed'
      };
    });
  }
})
  .directive('success', function() {
    return {
      restrict: 'E',
      transclude: true,
      template: '<div class="alert alert-success" ng-transclude></div>'
    }
  })
  .directive('danger', function() {
    return {
      restrict: 'E',
      transclude: true,
      template: '<div class="alert alert-danger" ng-transclude></div>'
    }
  });
