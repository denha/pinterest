angular.module('pinterestly')
  .directive('navbar', function () {
    return {
      templateUrl: 'navbar/navbar.html',
      restrict: 'E',
      controller: 'NavbarController'
    }
  })
  .controller('NavbarController', function($scope, $location, Session, Auth) {
    $scope.isCollapsed = true;
    $scope.Session = Session;
    $scope.Auth = Auth;
    $scope.menu = [{
      'title': 'Home',
      'link': '/pins'
    }];
    $scope.isActive = function(route) {
      return route === $location.path();
    }
  });
