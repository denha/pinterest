angular.module('pinterestly', ['ngRoute', 'ngCookies'])
  .config(function ($routeProvider, $httpProvider, $locationProvider) {
    $routeProvider.when('/create/pin', {
      templateUrl: 'createpin.html',
      controller: 'PinController'
    });
    $routeProvider.when('/pins', {
      templateUrl: 'wall.html',
      controller: 'WallController'
    });
    $routeProvider.when('/user/:uid', {
      templateUrl: 'wall.html',
      controller: 'WallController'
    });


    $routeProvider.otherwise({redirectTo: '/pins'});
    $httpProvider.interceptors.push(function ($q, $injector, Session) {
      return {
        // 'request': function (config) {
        //
        //   return config;
        // },
        'response': function (response) {
          // var Auth = $injector.get('Auth');
          Session.load()
          return response;
        }
      }
    })
    $locationProvider.html5Mode(true)
  })

;
