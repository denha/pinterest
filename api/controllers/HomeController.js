/**
 * HomeController
 *
 * @description :: Server-side logic for managing homes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var fs = require('fs');

module.exports = {
	index: function (req, res, next) {
    if (req.user) {
      res.cookie('user', JSON.stringify(req.user));
    } else {
      res.clearCookie('user');
    }
    fs.readFile('./app/index.html', function (err, info) {
      res.setHeader('Content-Type', 'text/html');
      res.send(info);
    });
      // next()
  }

};

