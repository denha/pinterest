'use strict';

/**
 * Policy to determine if the current user owns the pin
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 */
module.exports = function pinOwner(request, response, next) {
  // sails.log.verbose(__filename + ':' + __line + ' [Policy.addDataCreate() called]');

  Pin.findOne({id: request.param('id')}, function(err, pin) {
    if (pin && pin.createdUser.uid == request.user.uid) {
      next();
    } else {
      var error = new Error();
      error.message = 'Forbidden.';
      error.status = 403;
      next(error);
    }
  });
};
