'use strict';

/**
 * Policy to set necessary create data to body.
 *
 * @param   {Request}   request     Request object
 * @param   {Response}  response    Response object
 * @param   {Function}  next        Callback function
 */
module.exports = function addDataCreate(request, response, next) {
  // sails.log.verbose(__filename + ':' + __line + ' [Policy.addDataCreate() called]');

  if (request.user) {
    request.body.createdUser = request.user;
    // request.body.updatedUser = request.user;

    next();
  } else {
    var error = new Error();

    error.message = 'User not present.';
    error.status = 403;

    next(error);
  }
};
